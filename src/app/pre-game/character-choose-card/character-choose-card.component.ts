import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-character-choose-card',
  templateUrl: './character-choose-card.component.html',
  styleUrls: ['./character-choose-card.component.scss']
})
export class CharacterChooseCardComponent implements OnInit {

  @Input() charFoundName = '';
  @Input() charFoundSrc = '';

  @Output() charToFind = new EventEmitter();

  @ViewChild('charInput') charInput: ElementRef;

  ignoreBlur = false;

  constructor() { }

  ngOnInit() {
  }

  findCharacter(event) {
    event.stopPropagation();
    if(this.ignoreBlur){
      this.ignoreBlur = false
      return;
    }
    if(event.type == 'keyup'){
      this.ignoreBlur = true
    }
    
    const name = this.charInput.nativeElement.value.trim();
    this.charToFind.emit(name);
  }

}
