import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CharacterChooseCardComponent } from './character-choose-card.component';

describe('CharacterChooseCardComponent', () => {
  let component: CharacterChooseCardComponent;
  let fixture: ComponentFixture<CharacterChooseCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CharacterChooseCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharacterChooseCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
