import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AssetsModule } from '../assets/assets.module';

import { CharacterChooseCardComponent } from './character-choose-card/character-choose-card.component';
import { PreGameComponent } from './pre-game.component';
import { PreGameRoutingModule } from './pre-game-routing.module';
import { CharactersService } from './characters.service';


@NgModule({
  declarations: [
    PreGameComponent,
    CharacterChooseCardComponent
  ],
  imports: [
    CommonModule,
    AssetsModule,
    PreGameRoutingModule
  ],
  providers: [
    CharactersService
  ]
})
export class PreGameModule { }
