import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { CharactersService } from './characters.service';
import { Router } from '@angular/router';

import swal from 'sweetalert2';
import { DataStorage } from '../utils/data-storage';


@Component({
  selector: 'app-pre-game',
  templateUrl: './pre-game.component.html',
  styleUrls: ['./pre-game.component.scss']
})
export class PreGameComponent implements OnInit {
  isIntro = true;
  soundPlayerAction: any;

  @ViewChild('charPage') charPage: ElementRef;

  /* Characters choose variables */
  character1 = null;
  char1FoundName = '';
  char1FoundSrc = '';
  character2 = null;
  char2FoundName = '';
  char2FoundSrc = '';
  ready = false;

  constructor(
    private charactersService: CharactersService,
    private renderer: Renderer2,
    private router: Router,
    private dataStorage: DataStorage
  ) { }

  ngOnInit() {
    this.introSound();
    this.transitionCharactersPageDelay();
  }

  transitionCharactersPageDelay() {
    setTimeout(() => {
      this.isIntro = false;
      this.renderer.addClass(this.charPage.nativeElement, 'animated');
      this.renderer.addClass(this.charPage.nativeElement, 'fadeIn');
      this.renderer.addClass(this.charPage.nativeElement, 'pre-game-page');
    }, 5000);
  }

  introSound() {
    this.soundPlayerAction = {
      action: 'play',
      sound: 'intro'
    };
  }

  clickSound() {
    this.soundPlayerAction = {
      action: 'play',
      sound: 'click'
    };
  }

  checkReady() {
    if (this.character1 && this.character2) {
      this.ready = true;
    }
  }

  findCharacter1(name) {
    this.charactersService.findCharacter(name).subscribe(
      res => {
        if (res.data.results.length > 0) {
          this.character1 = res.data.results[0];
          this.char1FoundName = this.character1.name;
          this.char1FoundSrc = `${this.character1.thumbnail.path}/portrait_fantastic.${this.character1.thumbnail.extension}`;
          this.checkReady();
        } else {
          this.resetCharacter1();
          this.errorMsg('charNotFound');
        }
      },
      error => {
        this.resetCharacter1();
        this.errorMsg('charError');
      }
    );
  }

  resetCharacter1() {
    this.ready = false;
    this.character1 = null;
    this.char1FoundName = '';
    this.char1FoundSrc = '';
  }

  findCharacter2(name) {
    this.charactersService.findCharacter(name).subscribe(
      res => {
        if (res.data.results.length > 0) {
          this.character2 = res.data.results[0];
          this.char2FoundName = this.character2.name;
          this.char2FoundSrc = `${this.character2.thumbnail.path}/portrait_fantastic.${this.character2.thumbnail.extension}`;
          this.checkReady();
        } else {
          this.resetCharacter2();
          this.errorMsg('charNotFound');
        }
      },
      error => {
        this.resetCharacter2();
        this.errorMsg('charError');
      }
    );
  }

  resetCharacter2() {
    this.ready = false;
    this.character2 = null;
    this.char2FoundName = '';
    this.char2FoundSrc = '';
  }

  errorMsg(type) {
    switch (type) {
      case 'charNotFound': {
        swal(
          'Personagem não encontrado',
          'Busque novamente',
          'warning'
        );
        break;
      }
      case 'charError': {
        swal(
          'Erro',
          'Houve um problema ao buscar o personagem',
          'error'
        );
        break;
      }
    }
  }

  goToGame() {
    this.clickSound();
    const players = [
      {
        character: this.character1.name,
        thumbnail: this.char1FoundSrc,
        score: 0
      },
      {
        character: this.character2.name,
        thumbnail: this.char2FoundSrc,
        score: 0
      },
    ];
    this.dataStorage.set('players', players);
    setTimeout( () => {
      this.router.navigate(['../game']);
    }, 500);
  }

}
