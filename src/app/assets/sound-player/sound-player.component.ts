import { Component, OnInit, Input, ViewChild, ElementRef, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-sound-player',
  templateUrl: './sound-player.component.html',
  styleUrls: ['./sound-player.component.scss']
})
export class SoundPlayerComponent implements OnInit , OnChanges {

  @Input() action;
  @ViewChild('clickSound') clickSound: ElementRef;
  @ViewChild('introSound') introSound: ElementRef;
  @ViewChild('winSound') winSound: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.action) {
      this.audioAction(changes.action.currentValue);
    }
  }

  constructor() { }

  ngOnInit() {

  }

  audioAction(command: any) {
    if (!command.action) {
      return;
    }
    if (!command.sound) {
      return;
    }
    let audioElm;
    if (command.sound === 'intro') {
      audioElm = this.introSound;
    } else if (command.sound === 'click') {
      audioElm = this.clickSound;
    } else if (command.sound === 'win') {
      audioElm = this.winSound;
    }
    if (command.action === 'play') {
      this.play(audioElm);
    } else if (command.action === 'stop') {
      this.stop(audioElm);
    }
  }

  play(audioElm: ElementRef) {
    const playPromise = audioElm.nativeElement.play();
    if (playPromise !== null) {
        playPromise.catch(() => {
          console.error('Não foi possível tocar o audio');
          audioElm.nativeElement.play();
        });
    }
  }

  stop(audioElm: ElementRef) {
    audioElm.nativeElement.pause();
    audioElm.nativeElement.fastSeek(0);
  }

}
