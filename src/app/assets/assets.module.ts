import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { SoundPlayerComponent } from './sound-player/sound-player.component';
import { BackgroundDirective } from './background/background.directive';

@NgModule({
  declarations: [
    HeaderComponent,
    SoundPlayerComponent,
    BackgroundDirective
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    SoundPlayerComponent,
    BackgroundDirective
  ]
})
export class AssetsModule { }
