import { Directive, ElementRef, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBackground]'
})

export class BackgroundDirective {

  constructor(
    private renderer: Renderer2,
    private el: ElementRef
  ) {
    this.renderer.addClass(this.el.nativeElement, 'marvel-back-img');
   }
}
