import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-endmatch-card',
  templateUrl: './endmatch-card.component.html',
  styleUrls: ['./endmatch-card.component.scss']
})
export class EndmatchCardComponent implements OnInit {

  @Input() charSrc = '';

  constructor() { }

  ngOnInit() {
  }

}

