import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndmatchCardComponent } from './endmatch-card.component';

describe('EndmatchCardComponent', () => {
  let component: EndmatchCardComponent;
  let fixture: ComponentFixture<EndmatchCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndmatchCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndmatchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
