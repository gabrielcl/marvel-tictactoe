import { Component, OnInit } from '@angular/core';
import { DataStorage } from '../utils/data-storage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  players: any;
  nextToPlay: number;
  lastMatchFirstToPlay: number;
  ongame = true;

  score = [0, 0];
  winner: number;
  matchResult;

  soundPlayerAction: any = {};

  constructor(
    private dataStorage: DataStorage,
    private router: Router
  ) { 
    this.initializePlayers();
  }

  ngOnInit() {
    
  }

  initializePlayers() {
    const players = this.dataStorage.get('players');
    if (players) {
      this.players = players;
      this.lastMatchFirstToPlay = this.firstToPlay();
      this.nextToPlay = this.lastMatchFirstToPlay;
    } else {
      this.router.navigate(['/', 'pre-game']);
    }
  }

  firstToPlay() {
    return Math.floor((Math.random() * 10)) % 2;
  }

  playSound(command) {
    this.soundPlayerAction = command;
  }

  switchPlayer() {
    this.nextToPlay = Math.abs(this.nextToPlay - 1);
  }

  win(player) {
    this.playSound({
      action: 'play',
      sound: 'win'
    });
    this.winner = player;
    this.matchResult = 'win';
    this.ongame = false;
    this.score[player] += 1;
    
  }

  draw() {
    this.matchResult = 'draw';
    this.ongame = false;
  }

  continueGame() {
    this.playSound({
      action: 'play',
      sound: 'click'
    });
    this.nextToPlay = this.lastMatchFirstToPlay;
    this.ongame = true;
  }

}
