import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.scss']
})
export class ScoreBoardComponent implements OnInit {

  @Input() player1Img = '';
  @Input() player2Img = '';
  @Input() player1Score = 0;
  @Input() player2Score = 0;
  @Input() actualPlayer;

  constructor() { }

  ngOnInit() {
  }

}
