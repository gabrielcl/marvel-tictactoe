import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Blocks } from './blocks';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit {

  @Input() actualPlayerId;
  @Output() clickSound: EventEmitter<any> = new EventEmitter();
  @Output() switchPlayer: EventEmitter<any> = new EventEmitter();
  @Output() win: EventEmitter<any> = new EventEmitter();
  @Output() draw: EventEmitter<any> = new EventEmitter();
  
  pieces = [
    {
      img: 'assets/img/x.png',
      toSum: 1
    },
    {
      img: 'assets/img/o.png',
      toSum: 4
    },
    {
      img: 'assets/img/empty.png',
      toSum: 0
    }
  ];

  directionsMatch = {};
  
  blocks: Blocks = new Blocks();
  showScratchWin = false;
  dirWin = '';
  countClickBlock = 0;

  constructor() { 
    this.initializeDirectionsMatch();
  }


  ngOnInit() {

  }

  initializeDirectionsMatch() {
    this.directionsMatch["l0"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["l1"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["l2"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["c0"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["c1"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["c2"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["d1"] = {
      "player0": 0,
      "player1": 0
    };
    this.directionsMatch["d2"] = {
      "player0": 0,
      "player1": 0
    };
  }

  reset() {
    this.blocks.reset();
    Object.keys(this.directionsMatch).forEach( (d) => {
      this.directionsMatch[d].player0 = 0;
      this.directionsMatch[d].player1 = 0;
    });
  }

  emitClickSound() {
    const command = {
      action: 'play',
      sound: 'click'
    };
    this.clickSound.emit(command);
  }

  emitWin() {
    const winner = this.actualPlayerId;
    this.win.emit(winner);
  }

  onClickBlock(index, block, line, col) {
    if(!block.played) {
      this.emitClickSound();
      
      block.piece = this.actualPlayerId;
      const toSum = this.pieces[this.actualPlayerId].toSum;
      //sum line
      const lineDir = `l${block.position.l}`;
      console.log(' this.directionsMatch[lineDir][playerx]: ' +  this.directionsMatch[lineDir][`player${this.actualPlayerId}`]);
      this.directionsMatch[lineDir][`player${this.actualPlayerId}`] += toSum;
      if (this.verifyMatch(lineDir)) {
        return;
      }
      // sum column
      const colDir = `c${block.position.c}`;
      this.directionsMatch[colDir][`player${this.actualPlayerId}`] += toSum;
      if (this.verifyMatch(colDir)) {
        return;block.position.l + block.position.c === 4
      }
      // sum diagonal 1
      if (block.position.l === block.position.c) {
        this.directionsMatch['d1'][`player${this.actualPlayerId}`] += toSum;
        if (this.verifyMatch('d1')) {
          return;
        }
      }
      // sum diagonal 2
      console.log(`l${block.position.l} c${block.position.c}`);
      if (Math.abs(block.position.l - block.position.c) === 2 || (block.position.l === block.position.c && block.position.l + block.position.c === 2)) {
        this.directionsMatch['d2'][`player${this.actualPlayerId}`] += toSum;
        if (this.verifyMatch('d2')) {
          return;
        }
      }
      this.countClickBlock += 1;
      block.played = true;
      console.log('this.countClickBlock:', this.countClickBlock);
      if (this.countClickBlock === 9) {
        console.log('this.countClickBlock === 9 empate: ', this.countClickBlock);
        this.draw.emit(true);
        this.countClickBlock = 0;
        return;
      }
      this.switchPlayer.emit(true);
    }
  }

  verifyMatch(directionId) {
    console.log('verify match ',  directionId);
    console.log(`if(${this.directionsMatch[directionId][`player${this.actualPlayerId}`]} === 3 || ${this.directionsMatch[directionId][`player${this.actualPlayerId}`]} === 12)`);
    if(this.directionsMatch[directionId][`player${this.actualPlayerId}`] === this.pieces[this.actualPlayerId].toSum * 3) { //player wins
      this.dirWin = directionId;
      console.log('match ',  this.dirWin);
      this.showScratchWin = true;
      setTimeout(() => {
        this.emitWin();
        this.reset();
        this.dirWin = '';
        this.showScratchWin = false;
      }, 1500);
      return true;
    }
    return false
  }

}
