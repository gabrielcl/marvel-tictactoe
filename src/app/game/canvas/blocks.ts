export class Blocks {
    block = [
        {
          piece: 2,
          position: {
            l: 0,
            c: 0
          },
          played: false,
          borderCode: 'br bb'
        },
        {
          piece: 2,
          position: {
            l: 0,
            c: 1
          },
          played: false,
          borderCode: 'br bb'
        },
        {
          piece: 2,
          position: {
            l: 0,
            c: 2
          },
          played: false,
          borderCode: 'bb'
        },
        {
          piece: 2,
          position: {
            l: 1,
            c: 0
          },
          played: false,
          borderCode: 'br bb'
        },
        {
          piece: 2,
          position: {
            l: 1,
            c: 1
          },
          played: false,
          borderCode: 'br bb'
        },
        {
          piece: 2,
          position: {
            l: 1,
            c: 2
          },
          played: false,
          borderCode: 'bb'
        },
        {
          piece: 2,
          position: {
            l: 2,
            c: 0
          },
          played: false,
          borderCode: 'br '
        },
        {
          piece: 2,
          position: {
            l: 2,
            c: 1
          },
          played: false,
          borderCode: 'br '
        },
        {
          piece: 2,
          position: {
            l: 2,
            c: 2
          },
          played: false,
          borderCode: ''
        }
    ];

    reset() {
        this.block.forEach( b => {
            b.piece = 2;
            b.played = false;
        });
    }
}