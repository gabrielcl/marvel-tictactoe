import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScoreBoardComponent } from './score-board/score-board.component';
import { GameComponent } from './game.component';
import { CanvasComponent } from './canvas/canvas.component';
import { EndmatchCardComponent } from './endmatch-card/endmatch-card.component';
import { GameRoutingModule } from './game-routing.module';
import { AssetsModule } from '../assets/assets.module';

@NgModule({
  declarations: [
    ScoreBoardComponent, 
    GameComponent, 
    CanvasComponent, 
    EndmatchCardComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule,
    AssetsModule
  ]
})
export class GameModule { }
