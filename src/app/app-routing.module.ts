import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'pregame',
    loadChildren: './pre-game/pre-game.module#PreGameModule'
  },
  {
    path: 'game',
    loadChildren: './game/game.module#GameModule'
  },
  {
    path: '',
    redirectTo: '/pregame',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/pregame',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
