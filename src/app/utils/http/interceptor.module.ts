import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
} from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()

export class InterceptorModule implements HttpInterceptor {

  constructor() { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    req = req.clone({params: req.params.set('apikey', environment.apiKey)});
    // boilerplate to pipe response and errors
    return next.handle(req).pipe(
        tap((event: HttpEvent<any>) => {
            if (event instanceof HttpResponse) {

            }
        }),
        catchError( response => {
            if (response instanceof HttpErrorResponse) {
                if (!environment.production) {
                    console.log(response);
                }
                switch (response.status) {
                    case 401:
                    case 403:
                    case 405:
                    case 409: {
                        // do something
                    }
                }
            }
            return throwError(response);
      })
    );
  }
}
