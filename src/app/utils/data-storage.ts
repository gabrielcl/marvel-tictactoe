import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataStorage {
    data = {};
    public constructor() { }

    public set(key: string, value: Object) {
        this.data[key] = value;
    }

    public get(key: string) {
        return this.data[key];
    }

    public erase(key: string) {
        this.data[key] = null;
    }
}